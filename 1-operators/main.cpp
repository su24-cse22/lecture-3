#include <iostream>
#include <iomanip>
using namespace std;

int main() {

    /*

        Operators:
        +   addition
        -   subtraction
        *   multiplication
        /   division
        %   modulo

    */

    int x = 10;
    int y = 5;

    // addition
    int opAddition = x + y;
    cout << x << " + " << y << " = " << opAddition << endl;

    // subtraction
    int opSubtraction = x - y;
    cout << x << " - " << y << " = " << opSubtraction << endl;

    // multiplication
    int opMultiplication = x * y;
    cout << x << " * " << y << " = " << opMultiplication << endl;

    // division
    int opDivision = x / y;
    cout << x << " / " << y << " = " << opDivision << endl;

    // modulo: gives you the remainder of the division
    // Example 1: 10 % 5 => what is the remainder of 10 divided by 5? = 0
    // Example 2: 7 % 3 => what is the remainder of 7 divided by 3? = 1
    int opModulo = x % y;
    cout << x << " % " << y << " = " << opModulo << endl;

    return 0;
}