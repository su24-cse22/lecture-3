#include <iostream>
#include <iomanip>
using namespace std;

int main() {

    /*

        Create a program that takes in the price, tip percentage,
        and number of people attending dinner, and calculates the
        amount each person must pay to split the dinner costs evenly.

        Example Input:
        125.75
        15
        4

        Example Output:
        ------------------------------
        Dinner Bill Calculator
        ------------------------------
        Price:  $125.75
        Tip:    15%
        People: 4
        Total:  $144.61
        ------------------------------
        Amount per person: $36.15
        ------------------------------

    */

    float price, tipPercentage;
    int people;

    cin >> price;
    cin >> tipPercentage;
    cin >> people;

    float tipAmount = price * (tipPercentage / 100);
    float totalPrice = price + tipAmount;
    float splitAmount = totalPrice / people;

    // make all the floating point numbers to have 2 digits after the decimal
    cout << fixed << setprecision(2);

    cout << "------------------------------" << endl;
    cout << "Dinner Bill Calculator" << endl;
    cout << "------------------------------" << endl;
    cout << "Price:  $" << price << endl;
    cout << "Tip:    " << tipPercentage << "%" << endl;
    cout << "People: " << people << endl;
    cout << "Total:  $" << totalPrice << endl;
    cout << "------------------------------" << endl;
    cout << "Amount per person: $" << splitAmount << endl;
    cout << "------------------------------" << endl;

    return 0;
}