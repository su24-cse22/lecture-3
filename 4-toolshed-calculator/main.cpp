#include <iostream>
using namespace std;

int main() {

    /*

        Create a program that takes in the width, length,
        and height dimensions of a toolshed, and generates
        a quote for the costs of building it.

        Floor Price: $10.75 sq/ft
        Wall Price:  $7.25 sq/ft
        Roof Price:  $12.50 sq/ft

        Example Input:
        12
        14
        10

        Example Output:
        ------------------------------
        Tool Shed Quote
        ------------------------------
        Dimensions: 12 x 14 x 10
        ------------------------------
        Floor Material: $1806
        Wall Material:  $3770
        Roof Material:  $2100
        ------------------------------
        TOTAL: $7676
        ------------------------------

    */

    float width, length, height;
    cin >> width;
    cin >> length;
    cin >> height;

    float floorPricePerSqFt = 10.75;
    float floorArea = width * length;
    float floorCost = floorArea * floorPricePerSqFt;

    float wallPricePerSqFt = 7.25;
    float wallArea = (2 * width * height) + (2 * length * height);
    float wallCost = wallArea * wallPricePerSqFt;

    float roofPricePerSqFt = 12.50;
    float roofArea = width * length;
    float roofCost = roofArea * roofPricePerSqFt;

    float totalCost = floorCost + wallCost + roofCost;

    cout << "------------------------------" << endl;
    cout << "Tool Shed Quote" << endl;
    cout << "------------------------------" << endl;
    cout << "Dimensions: " << width << " x " << length << " x " << height << endl;
    cout << "------------------------------" << endl;
    cout << "Floor Material: $" << floorCost << endl;
    cout << "Wall Material:  $" << wallCost << endl;
    cout << "Roof Material:  $" << roofCost << endl;
    cout << "------------------------------" << endl;
    cout << "TOTAL: $" << totalCost << endl;
    cout << "------------------------------" << endl; 

    return 0;
}