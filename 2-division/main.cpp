#include <iostream>
#include <iomanip>
using namespace std;

int main() {

    /*

        Integer Division
        - result of the division is truncated
        - requires both operands to be of type `int`

        Float Point Division
        - result is not truncated
        - requires at least one operand to be of type `float`

    */

    // integer division: both x and y are of type `int`
    int x = 7;
    int y = 3;
    float intDivision = x / y;

    cout << "Integer Division: " << x << " / " << y << " = " << intDivision << endl; 

    // floating point division: at least one of the operands (b) is of type `float`
    int a = 7;
    float b = 3;
    float floatDivision = a / b;
    
    cout << "Floating Point Division: " << a << " / " << b << " = " << floatDivision << endl;

    // floating point division: both p and q are of type `float`
    float p = 7;
    float q = 3;
    float floatDivision_2 = p / q;
    
    cout << "Floating Point Division: " << p << " / " << q << " = " << floatDivision_2 << endl;


    // floating point division by casting: although g and h are both of type `int`, we typecase g into a `float`
    int g = 7;
    int h = 3;
    float floatDivision_3 = (float) g / h;

    cout << "Floating Point Division: " << g << " / " << h << " = " << floatDivision_3 << endl;


    return 0;
}